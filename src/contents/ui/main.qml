import QtQuick 2.7
import org.kde.kirigami 2.4 as Kirigami
import QtQuick.Controls 2.0 as Controls

import org.kde.rantdev 0.1

Kirigami.ApplicationWindow {
    id: root

    title: i18n("rantdev")

    contextDrawer: Kirigami.ContextDrawer {
        id: contextDrawer
    }

    pageStack.initialPage: Qt.resolvedUrl("RantsPage.qml")
}
