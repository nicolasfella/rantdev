import QtQuick 2.7
import org.kde.kirigami 2.4 as Kirigami
import QtQuick.Controls 2.0 as Controls

import org.kde.rantdev 0.1

Kirigami.ScrollablePage {
    title: i18n("rantdev")

    Kirigami.CardsListView {

        model: RantsModel {}

        delegate: Kirigami.Card {

            contentItem: Controls.Label {
                text: rantText
                width: parent.width
                wrapMode: Text.Wrap
            }
        }
    }
}
