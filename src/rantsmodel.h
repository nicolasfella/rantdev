/*
 * Copyright 2020  Nicolas Fella <nicolas.fella@gmx.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RANTSMODEL_H
#define RANTSMODEL_H

#include <QAbstractListModel>
#include <QJsonDocument>
#include <QJsonArray>

class QNetworkReply;

class RantsModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum Roles {
        TextRole = Qt::UserRole + 1
    };

public:
    explicit RantsModel();
    ~RantsModel();

    int rowCount(const QModelIndex & parent) const override;
    QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex & index, int role) const override;

private:
    void onReply(QNetworkReply *reply);

    QJsonDocument m_json;
    QJsonArray m_rants;
};

#endif // RANTSMODEL_H
