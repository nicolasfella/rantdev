/*
 * Copyright 2020  Nicolas Fella <nicolas.fella@gmx.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rantsmodel.h"

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QJsonObject>

#include <QDebug>

RantsModel::RantsModel()
{
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    connect(manager, &QNetworkAccessManager::finished, this, &RantsModel::onReply);

    manager->get(QNetworkRequest(QUrl("https://devrant.com/api/devrant/rants?app=3&sort=recent&range=day&limit=20&skip=0")));
}

RantsModel::~RantsModel()
{
}

void RantsModel::onReply(QNetworkReply *reply)
{
    QByteArray data;
    data = reply->readAll();

    m_json = QJsonDocument::fromJson(data);
    QJsonObject obj = m_json.object();

    beginResetModel();
    m_rants = obj.value(QStringLiteral("rants")).toArray();
    endResetModel();
}

QVariant RantsModel::data(const QModelIndex& index, int role) const
{
    int row = index.row();

    switch (role) {
        case Roles::TextRole:
            return m_rants[row].toObject().value("text");
        default:
            return QStringLiteral("deadbeef");
    }
}

int RantsModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent)
    return m_rants.count();
}

QHash<int, QByteArray> RantsModel::roleNames() const
{
    return {
        {Roles::TextRole, "rantText"}
    };
}

